<?php

use Illuminate\Http\Request;



Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();

});
//deps
Route::get('/departments', 'DepartmentController@index');
Route::get('/dep/edit/{id}', 'DepartmentController@edit');
Route::post('/dep/update/{id}', 'DepartmentController@update');
Route::post('/dep/store', 'DepartmentController@store');
Route::delete('/dep/destroy/{id}', 'DepartmentController@destroy');

//wards
Route::get('/wards', 'WardController@index');
Route::post('/wards/store', 'WardController@store');
Route::get('/wards/edit/{id}', 'WardController@edit');
Route::post('/wards/update/{id}', 'WardController@update');

//users
Route::get('/users', 'UsersController@index');
Route::post('/users/store', 'UsersController@store');
