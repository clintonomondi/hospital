<?php

namespace App\Http\Controllers;

use App\Ward;
use Illuminate\Http\Request;

class WardController extends Controller
{

    public function index()
    {
       $wards=Ward::all();
       return response()->json($wards);
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        $ward=Ward::create($request->all());
        return response()->json("Wards inserted successfully");
    }


    public function show(Ward $ward)
    {
        //
    }


    public function edit($id)
    {
        $ward=Ward::find($id);
        return response()->json($ward);
    }


    public function update(Request $request,$id)
    {
        $ward=Ward::find($id);
        $ward->update($request->all());
        return response()->json("Wards updated successfully");
    }


    public function destroy(Ward $ward)
    {
        //
    }
}
