<?php

namespace App\Http\Controllers;

use App\Department;
use Illuminate\Http\Request;

class DepartmentController extends Controller
{

    public function index()
    {
        $deps = Department::all();
        return response()->json($deps);
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        $dep=Department::create($request->all());
        return response()->json([
            "message" => "Request Success",
        ], 200);
    }


    public function show(Department $department)
    {
        //
    }



    public function edit($id)
    {
        $dep=Department::find($id);
        return response()->json($dep);
    }



    public function update(Request $request,$id)
    {
        $dep=Department::find($id);
        $dep->update($request->all());
        return response()->json('Successfully Updated');
    }



    public function destroy($id)
    {
        $dep=Department::findOrFail($id);
        $dep->delete();
        return response()->json('Successfully deleted');
    }
}
