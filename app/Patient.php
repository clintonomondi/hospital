<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    protected $fillable=['name','gender','contact','ward_id','nurse_id','bed'];

    public function ward(){
        return $this->belongsTo(Ward::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }
}
