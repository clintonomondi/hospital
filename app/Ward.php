<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ward extends Model
{ 
    protected $fillable=['name'];

    public function patient(){
        return $this->hasmany(Patient::class);
    }
}
