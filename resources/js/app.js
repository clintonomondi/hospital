import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);
import VueAxios from 'vue-axios';
import axios from 'axios';
Vue.use(VueAxios, axios);

import VueSweetalert2 from 'vue-sweetalert2';
const options = {
    confirmButtonColor: '#41b882',
    cancelButtonColor: '#ff7674'
  }

  Vue.use(VueSweetalert2, options)

import App from "./views/App";
import Home from "./views/Home";
import Doctors from "./views/users/Doctors.vue";
import Patients from "./views/patients/Patients.vue";
import AddPatient from "./views/patients/AddPatient.vue";
import Login from "./users/Login.vue";
import Profile from "./users/Profile.vue";
import EditPatient from "./views/patients/EditPatient.vue";
import Departments from "./views/departments/Departments";
import  Adddoctor from "./views/users/Adddoctor";
import  Editdoctor from "./views/users/Editdoctor";
import Adddepartment from "./views/departments/Adddepartment";
import  Editdep from "./views/departments/Editdep";
import Wards from "./views/wards/wards.vue";
import Addward from "./views/wards/addward.vue";
import Editward from "./views/wards/editward.vue";

const router = new VueRouter({
    mode: "history",
    routes: [
        {
            path: "/",
            name: "home",
            component: Home
        },
        {
            path: "/doctors",
            name: "doctors",
            component: Doctors
        },
        {
            path: "/patients",
            name: "patients",
            component: Patients
        },
        {
            path: "/addpatient",
            name: "addpatient",
            component: AddPatient
        },
        {
            path: "/auth",
            name: "login",
            component: Login
        },
        {
            path: "/profile",
            name: "profile",
            component: Profile
        },
        {
            path: "/editpatient",
            name: "editpatient",
            component: EditPatient
        },
        {
            path: "/departments",
            name: "departments",
            component: Departments
        },
        {
            path: "/adddoctor",
            name: "adddoctor",
            component: Adddoctor
        },
        {
            path: "/editdoctor",
            name: "editdoctor",
            component: Editdoctor
        },
        {
            path: "/dep/add",
            name: "adddepartment",
            component: Adddepartment
        },
        {
            path: "/editdep/:id",
            name: "editdep",
            component: Editdep
        },
        {
            path: "/wards",
            name: "wards",
            component: Wards
        },
        {
            path: "/addward",
            name: "addward",
            component: Addward
        },
        {
            path: "/editward/:id",
            name: "editward",
            component: Editward
        }

    ]
});

const app = new Vue({
    el: "#app",
    components: { App },
    router
});
